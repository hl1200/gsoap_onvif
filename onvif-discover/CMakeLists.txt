cmake_minimum_required(VERSION 3.10)
project(onvif-discover)

#set(CMAKE_CXX_STANDARD 11)

add_definitions("-Wall -g")
aux_source_directory(src SRC_LIST)

message("SRC_LIST: ${SRC_LIST}")

add_executable(onvif-discover  ${SRC_LIST})
