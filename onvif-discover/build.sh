#!/bin/sh

rm -rf ./app
mkdir ./app
mkdir ./app/src
mkdir ./app/build
cd ./app

cp ../soap/* ./src

cp ../CMakeLists.txt ./

cd ./build

cmake ..
make