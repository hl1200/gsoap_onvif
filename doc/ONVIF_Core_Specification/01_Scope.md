# 简介

```
This specification defines procedures for communication between network clients and devices. This new set of
specifications makes it possible to build e.g. network video systems with devices and receivers from different
manufacturers using common and well defined interfaces. The functions defined in this specification covers
discovery, device management and event framework.
Supplementary dedicated services as e.g. media configuration, real-time streaming of audio and video, Pan,
Tilt and Zoom (PTZ) control, video analytics as well as control, search and replay of recordings are defined
in separate documents.
The management and control interfaces defined in this standard are described as Web Services. This standard
also contains full XML schema and Web Service Description Language (WSDL) definitions.
In order to offer full plug-and-play interoperability, the standard defines procedures for device discovery. The
device discovery mechanisms in the standard are based on the WS-Discovery specification with extensions.
本规范定义了网络客户端和设备之间的通信过程。这套新的规范使得使用来自不同制造商的设备和接收器使用通用且定义良好的接口来
构建例如网络视频系统成为可能。本规范中定义的功能包括发现、设备管理和事件框架。
补充专用服务，如媒体配置、音频和视频的实时流媒体、平移、倾斜和缩放（PTZ）控制、视频分析以及录音的控制、搜索和回放，
在单独的文档中进行了定义。
本标准中定义的管理和控制接口被描述为Web服务。该标准还包含完整的XML模式和Web服务描述语言（WSDL）定义。
为了提供完全的即插即用互操作性，该标准定义了设备发现程序。该标准中的设备发现机制基于具有扩展的WS-discovery规范。
```
## 注意
core这个文档只是定义了通用的服务接口，例如发送和接受，具体的业务在专用的文档里面