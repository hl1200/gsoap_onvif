```
INTRODUCTION
引言

The goal of this specification is to provide the common base for a fully interoperable network implementation
comprised of products from different network vendors. This standard describes the network model, software
interfaces, software data types and data exchange patterns. The standard reuses existing relevant standards
where available, and introduces new specifications only where necessary.
本规范的目标是为由不同网络供应商的产品组成的完全可互操作的网络实现提供公共基础。
本标准描述了网络模型、软件接口、软件数据类型和数据交换模式。
该标准在可用的情况下重复使用现有的相关标准，并仅在必要时引入新的规范。

This is the ONVIF core specification. It is accompanied by a set of computer readable interface definitions:
• ONVIF Schema [ONVIF Schema]
• ONVIF Device Service WSDL [ONVIF DM WSDL]
• ONVIF Event Service WSDL [ONVIF Event WSDL]
• ONVIF Topic Namespace XML [ONVIF Topic Namespace]
这是ONVIF核心规范。它附带了一组计算机可读接口定义：
• ONVIF Schema [ONVIF Schema] 架构
• ONVIF Device Service WSDL [ONVIF DM WSDL] 设备服务的Web服务描述语言
• ONVIF Event Service WSDL [ONVIF Event WSDL] 事件服务的Web服务描述语言
• ONVIF Topic Namespace XML [ONVIF Topic Namespace] 顶层命名空间的XML

The purpose of this document is to define the ONVIF specification framework, and is divided into the following sections:
Specification Overview: Gives an overview of the different specification parts and how they are related to each other.
Web Services Frame Work: Offers a brief introduction to Web Services and the Web Services basis for the ONVIF specifications.
IP Configuration: Defines the ONVIF network IP configuration requirements.
Device Discovery: Describes how devices are discovered in local and remote networks.
Device Management: Defines the configuration of basics like network and security related settings.
Event Handling: Defines how to subscribe to and receive notifications (events) from a device.
Security Section: Defines the message level security requirements on ONVIF compliant implementations.
本文件旨在定义ONVIF规范框架，分为以下几节：
规范概述：概述不同规范部分及其相互关系。
Web服务框架工作：简要介绍Web服务和ONVIF规范的Web服务基础。
IP配置：定义ONVIF网络IP配置要求。
设备发现：描述如何在本地和远程网络中发现设备。
设备管理：定义基本配置，如网络和安全相关设置。
事件处理：定义如何订阅设备和从设备接收通知（事件）。
安全部分：定义符合ONVIF的实现的消息级安全要求。
```