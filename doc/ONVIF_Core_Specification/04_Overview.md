```
4 Overview
This specification originated from network video use cases covering both local and wide area network scenarios
and has been extended to cover generic IP device use cases. The specification defines a core set of interface
functions for configuration and operation of network devices by defining their server side interfaces.
This standard covers device discovery, device configuration as well as an event framework.
All services share a common XML schema and all data types are provided in [ONVIF Schema]. The different
services are defined in the respective sections and service WSDL documents.
4概述
本规范源于涵盖局域网和广域网场景的网络视频用例并且已经扩展到涵盖通用IP设备使用情况。规范定义了一组核心接口
用于通过定义网络设备的服务器侧接口来配置和操作网络设备的功能。
该标准涵盖设备发现、设备配置以及事件框架。
所有服务共享一个通用的XML模式，所有数据类型都在[ONVIF模式]中提供。
不同的服务是在相应的部分和服务WSDL文档中定义的。

4.1 Web Services
The term Web Services is the name of a standardized method of integrating applications using open, platform
independent Web Services standards such as XML, SOAP 1.2 [Part 1] and WSDL1.1 over an IP network. XML
is used as the data description syntax, SOAP is used for message transfer and WSDL is used for describing
the services.
This framework is built upon Web Services standards. All configuration services defined in the standard are 
expressed as Web Services operations and defined in WSDL with HTTP as the underlying transport mechanism.
Figure 1givesanoverview of thebasicprinciples fordevelopmentbasedon Web Services. Theserviceprovider
(device) implements the ONVIF service or services. The service is described using the XML-based WSDL.
Then, the WSDL is used as the basis for the service requester (client) implementation/integration. Client-side
integration is simplified through the use of WSDL compiler tools that generate platform specific code that can
be used by the client side developer to integrate the Web Service into an application.
The Web Service provider and requester communicate using the SOAP message exchange protocol. SOAP is
a lightweight, XML-based messaging protocol used to encode the information in a Web Service request and in
a response message before sending them over a network. SOAP messages are independent of any operating
system or protocol and may be transported using a variety of Internet protocols. This ONVIF standard defines
conformant transport protocols for the SOAP messages for the described Web Services.
The Web Serviceoverview sectionintroduces intothegeneral ONVIF servicestructure, thecommanddefinition
syntax in the specification, error handling principles and the adopted Web Service security mechanisms.
To ensure interoperability, all ONVIF services follow the Web Services Interoperability Organization (WS-I)
basic profile 2.0 recommendations and use the document/literal wrapped pattern.

术语Web服务是使用开放平台集成应用程序的标准化方法的名称独立的Web服务标准，
如IP网络上的XML、SOAP 1.2[第1部分]和WSDL1.1。XML语言用作数据描述语法，
SOAP用于消息传输，WSDL用于描述服务。
这个框架是建立在Web服务标准之上的。标准中定义的所有配置服务表示为Web服务操作，
并以HTTP作为底层传输机制在WSDL中定义。
图1概述了基于Web服务开发的基本原则。服务提供商（设备）实现ONVIF服务。
该服务是使用基于XML的WSDL描述的。然后，WSDL被用作服务请求者（客户端）实现/集成的基础。
客户端通过使用WSDL编译器工具来简化集成，这些工具可以生成特定于平台的代码由客户端开发人员用来将Web服务集成到应用程序中。
Web服务提供者和请求者使用SOAP消息交换协议进行通信。SOAP是一种轻量级的、基于XML的消息传递协议，
用于对Web服务请求中的信息和在通过网络发送之前发送响应消息。SOAP消息独立于任何操作
系统或协议，并且可以使用各种互联网协议进行传输。本ONVIF标准定义所描述的Web服务的SOAP消息的一致传输协议。
Web服务概述部分介绍了ONVIF服务的一般结构、命令定义
规范中的语法、错误处理原则以及所采用的Web Service安全机制。
为了确保互操作性，所有ONVIF服务都遵循Web服务互操作性组织（WS-I）
基本概要文件2.0建议，并使用文档/文字包装模式。


```