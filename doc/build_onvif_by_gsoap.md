这里主要参考了下面这篇文章：

[使用gSOAP工具生成onvif框架代码](https://www.cnblogs.com/big-devil/p/7625763.html)

[Onvif协议：实现Probe命令来进行设备发现（discover）](https://blog.csdn.net/zhizhengguan/article/details/109288107)


[gsoap官网](https://www.genivia.com/index.html)

[onvif网络接口规范](https://www.onvif.org/ch/profiles/specifications/)

[服务端例子](https://blog.csdn.net/weixin_44362642/article/details/86750172)

一般只需要

remotediscovery.wsdl ：用于发现设备

devicemgmt.wsdl ：用于获取设备参数

media.wsdl：用于获取视频流地址

ptz.wsdl：用于设备的PTZ控制


在ubuntu里面直接可以安装gsoap

```
sudo apt-get install gsoap
```

生成头文件

```
sh ./onvif_head.sh
```

生成接口源代码

```
sh gen_code.sh
```

```
如果出现错误
Critical error: #import: Cannot open file "custom/duration.h" for reading.

加上-I/usr/share/gsoap 指明路径
```